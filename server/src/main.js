const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const axios = require('axios')
const jwt = require('jsonwebtoken')
const app = express()
const port = 8080

const TOKEN_SECRET = '0a659efa397032fe18833a2be357210123c27fbc77cb717c9c9c97dc65c6761aba9abece607877b6b98cbc952d94d81c9bd2c92b5eb38b71c11a45d95cc966b8'

const authenticated = async (req, res, next) => {
    const auth_header = req.headers['authorization']
    const token = auth_header && auth_header.split(' ')[1]
    if(!token)
        return res.sendStatus(401)
    jwt.verify(token, TOKEN_SECRET, (err, info) => {
        if(err) return res.sendStatus(403)
        req.username = info.username
        next()
    })
}

app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/api/info', authenticated, (req, res) => {
    res.send({ok: 1, username: req.username})
  })

app.post('/api/login', bodyParser.json(), async ( req, res) => {
    let token = req.body.token
    let result = await axios.get('https://graph.facebook.com/me', {
        params: { 
            fields: 'id,name,email' ,
            access_token: token
        }
    })
    if(!result.data.id){
        res.sendStatus(403)
        return
    }
    let data = {
        username: result.data.email
    }
    let access_token = jwt.sign(data, TOKEN_SECRET, {expiresIn: '1800s'})
    res.send({access_token, username: data.username})
})

app.listen(port, () => {
  console.log(`Server app listening on port ${port}`)
})